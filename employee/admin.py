from django.contrib import admin

from .models import Employee, EmployeeReport

admin.site.register(Employee)
admin.site.register(EmployeeReport)

# Register your models here.
