from rest_framework import serializers

from .models import Employee, EmployeeReport

class EmployeeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Employee
        fields = '__all__'
        
        
class EmployeeReportSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = EmployeeReport
        fields = '__all__'
        