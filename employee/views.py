import csv 

from dateutil.parser import parse

from django.shortcuts import render, get_object_or_404, render
from .models import EmployeeReport, Employee, Report
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import EmployeeReportSerializer
from django.core.files.storage import FileSystemStorage

    

class EmployeeReportTable(APIView):
    JOB_GROUP_A = 20
    JOB_GROUP_B = 30
    
    def get(self, request):
        employee = EmployeeReport.objects.all().order_by('employee_id')
        serializer = EmployeeReportSerializer(employee, many=True)
        context = {"employee_report" : employee}
        return render(request, 'employee/index.html', context)

    def post(self, request):
        
        if request.method == 'POST':
            myfile = request.FILES['myfile']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            first_read = True
            employee_data = self._list_employee_data()
            
            try:
                with open(filename, "rb") as f:
                    reader = csv.reader(f, delimiter="\t")
                    
                    for line in reversed(list(reader)):
                        line = line[0].split(',')
                        if first_read:
                            report_exist = self._check_report_id(int(line[1]))
                            if report_exist:
                                return render(request, 'employee/index.html', {'uploaded_file_url': 'file already uploaded', 'employee_report': employee_data} )
                            first_read = False
                        print line    
                        if line[2].isdigit():
                            employee = Employee()
                            employee.work_date = parse(line[0])
                            employee.hours_worked = float(line[1])
                            employee.employee_id = int(line[2])
                            employee.job_group = line[3]
                            employee.save()
                            self._save_employee_report(employee)
            except Exception as e:
                return render(request, 'employee/index.html', {'uploaded_file_url': 'file is not csv', 'employee_report': employee_data} ) 

            uploaded_file_url = fs.url(filename)
            employee_data = self._list_employee_data()

            return render(request, 'employee/index.html', {
                'uploaded_file_url': uploaded_file_url,
                "employee_report": employee_data
            })
        return render(request, 'employee/index.html')        
        
    def _save_employee_report(self, employee):
        existing_employee = EmployeeReport.objects.filter(employee_id=employee.employee_id)
        employee.work_date = employee.work_date.date()
        
        if existing_employee and len(existing_employee) > 0:
            existing_employee = existing_employee[0]
            
            if existing_employee.from_date > employee.work_date:
                existing_employee.from_date = employee.work_date
            elif existing_employee.to_date < employee.work_date:
                existing_employee.to_date = employee.work_date
                
            existing_employee.amount_paid += employee.hours_worked * self._check_group(employee.job_group)
            existing_employee.save()
            
            return True
        
        new_employee = EmployeeReport()
        new_employee.employee_id = employee.employee_id
        new_employee.from_date = employee.work_date
        new_employee.to_date = employee.work_date
        new_employee.job_group = employee.job_group
        new_employee.amount_paid = employee.hours_worked * self._check_group(employee.job_group)
        new_employee.save()
        
        return True        
    
    def _check_group(self, group):
        return self.JOB_GROUP_A if group == "A" else self.JOB_GROUP_B
    
    
    def _check_report_id(self, report_id):
        report = Report.objects.filter(report_id=report_id)
        
        if report:
            return True
        else:
            report = Report()
            report.report_id = report_id
            report.save()
            return False

    def _list_employee_data(self):
        employee = EmployeeReport.objects.all().order_by('employee_id')
        return employee
