from __future__ import unicode_literals

from django.db import models

class Employee(models.Model):
    employee_id = models.BigIntegerField()
    work_date = models.DateField()
    hours_worked = models.BigIntegerField()
    job_group = models.CharField(max_length=1)

    def __str__(self):
        return self.job_group
        
class EmployeeReport(models.Model):
    employee_id = models.BigIntegerField()
    from_date = models.DateField()
    to_date = models.DateField()
    amount_paid = models.FloatField()
    job_group = models.CharField(max_length=1)

    def __str__(self):
        return self.job_group
    
class Report(models.Model):
    report_id = models.BigIntegerField()
